# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains dumps of database for mysql

### How do I get set up? ###

* clone this repository to your local
* open mysql workbench
* Create a database name as linkrosteruserservice_dev
* From the menu bar select server tab
* select data import
* Browse the directory to folder in cloned repository and select Dump20220111 folder
* Now select on Import Progress tab and click on Start import button
* once it's done execute these queries
INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
